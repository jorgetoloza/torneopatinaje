﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/Site1.Master" AutoEventWireup="true" CodeBehind="deportistas.aspx.cs" Inherits="TorneoPatinaje.app.atletas" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>    
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="scripts/deportistasCtrl.js"></script>
    <script src="scripts/script.js"></script>
    <h1>Deportistas en el torneo</h1>
    <div id="deportistas-container" ng-controller="deportistasCtrl">
        <div class="deportista" ng-repeat="deportista in deportistas" data-deportista-id="{{deportista.deportistaID}}">
            <div class="btn-delete flex">
                Eliminar <div class="ico-delete"></div>
            </div>
            <div class="deportista-img"></div>
            <span class="nombre">{{deportista.primerNombre}} {{deportista.primerApellido}}</span>
            <div class="flex genero-container">
                Genero: {{deportista.genero}} 
                <div ng-if="deportista.genero == 'F'" class="ico-genero female"></div>
                <div ng-if="deportista.genero == 'M'" class="ico-genero male"></div>
            </div>
            <span>Fecha de nacimiento: {{deportista.fechaNacimiento | date: 'dd/MM/yyyy'}}</span>
            <span>Dorsal:  {{deportista.dorsal}}</span>
            <span>{{deportista.paisARepresentar}}</span>
        </div>
        <%--<div class="deportista">
            <div class="btn-delete flex">
                Eliminar <div class="ico-delete"></div>
            </div>
            <div class="deportista-img"></div>
            <span class="nombre">Jake el perro</span>
            <div class="flex genero-container">
                Genero: Masculino 
                <div class="ico-genero male"></div>
            </div>
            <span>01/05/1983</span>
            <span>Dorsal: 13</span>
            <span>Colombia</span>
        </div>--%>
    </div>
</asp:Content>
