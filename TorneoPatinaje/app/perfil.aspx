﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/Site1.Master" AutoEventWireup="true" CodeBehind="perfil.aspx.cs" Inherits="TorneoPatinaje.app.WebForm2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script runat="server">
        String id;
        void Page_Load(object sender, EventArgs e){
            id = Request.QueryString["deportistaID"];
        }
    </script>
    <script src="scripts/perfilCtrl.js"></script>
    <div ng-controller="perfilCtrl">
        <div id="cabecera" class="flex">
            <h1>Editar perfil</h1>
            <div class="buttons-container flex">
                <div class="btn-save flex" ng-click="guardar()">
                    <span >Guardar</span>
                </div>
                <div class="btn-discard flex">
                    <a href="deportistas.aspx">Descartar los cambios</a>
                </div>
                <div class="btn-delete flex" ng-click="eliminar()">
                    <div class="ico-delete"></div>
                    <span>Eliminar del torneo</span>
                </div>
            </div>
        
        </div>
    
        <div id="perfil" data-id="<%=id %>">
            <div id="perfil-img"></div>
            <div id="info">
                <div class="form-control">
                    <label>Primer nombre</label>
                    <input type="text" name="firstName" ng-model="deportista.primerNombre"/>
                </div>
                <div class="form-control">
                    <label>Segundo nombre</label>
                    <input type="text" name="secondName"  ng-model="deportista.segundoNombre"/>
                </div>
                <div class="form-control">
                    <label>Primer apellido</label>
                    <input type="text" name="lastName" ng-model="deportista.primerApellido"/>
                </div>
                <div class="form-control">
                    <label>Segundo apellido</label>
                    <input type="text" name="secondSurname" ng-model="deportista.segundoApellido"/>
                </div>
                <div class="form-control">
                    <label>Genero</label>
                    <input type="text" name="genero" ng-model="deportista.genero" value="{{deportista.genero}}"/>
                </div>
                <div class="form-control">
                    <label>Fecha de nacimiento</label>
                    <input type="date" name="birdthDate" value="{{deportista.fechaNacimento | date : "yyyy-MM-dd"}}" ng-model="deportista.fechaNacimiento"/>
                </div>
            </div>
        </div>
    </div>
    
</asp:Content>
