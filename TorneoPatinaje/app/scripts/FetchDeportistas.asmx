﻿<%@ WebService Language="C#" Class="FetchDeportistas" %>
using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Data.SqlClient;
using System.Text;
using System.Web.Script.Services;


[WebService(Namespace = "http://microsoft.com/webservices/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[ScriptService]


public class FetchDeportistas : WebService {
    string connect = "Server=DESKTOP-9ML4V9O;Database=Patinaje;Trusted_Connection=True";
    [WebMethod]
    public System.Collections.ArrayList GetDeportistas() {
        System.Collections.ArrayList arreglo = new System.Collections.ArrayList();
        string query = "SELECT * FROM InformacionDeportistas";
        StringBuilder sb = new StringBuilder();
        using (SqlConnection conn = new SqlConnection(connect)) {
            using (SqlCommand cmd = new SqlCommand(query, conn)) {
                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                if (rdr.HasRows) {
                    while (rdr.Read()) {
                        Deportista deportista = new Deportista();
                        deportista.deportistaID = (int)rdr["DeportistaID"];
                        deportista.primerNombre = rdr["PrimerNombre"].ToString();
                        deportista.segundoNombre = rdr["SegundoNombre"].ToString();
                        deportista.primerApellido = rdr["PrimerApellido"].ToString();
                        deportista.segundoApellido = rdr["segundoApellido"].ToString();
                        deportista.fechaNacimiento = (DateTime)rdr["FechaNacimiento"];
                        deportista.genero = rdr["genero"].ToString();
                        deportista.dorsal = (int)rdr["dorsal"];
                        deportista.peso = (decimal)rdr["peso"];
                        deportista.altura = (decimal)rdr["altura"];
                        deportista.paisARepresentar = rdr["paisARepresentar"].ToString();
                        arreglo.Add(deportista);
                    }
                }
            }
            return arreglo;
        }
    }

    [WebMethod]
    public Deportista GetDeportista(string ID) {
        string query = "EXEC DeportistaByID " + ID;
        StringBuilder sb = new StringBuilder();
        Deportista deportista = new Deportista();
        using (SqlConnection conn = new SqlConnection(connect)) {
            using (SqlCommand cmd = new SqlCommand(query, conn)) {
                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                if (rdr.HasRows) {
                    while (rdr.Read()) {
                        deportista.deportistaID = (int)rdr["DeportistaID"];
                        deportista.primerNombre = rdr["PrimerNombre"].ToString();
                        deportista.segundoNombre = rdr["SegundoNombre"].ToString();
                        deportista.primerApellido = rdr["PrimerApellido"].ToString();
                        deportista.segundoApellido = rdr["segundoApellido"].ToString();
                        deportista.fechaNacimiento = (DateTime)rdr["FechaNacimiento"];
                        deportista.genero = rdr["genero"].ToString();
                        deportista.dorsal = (int)rdr["dorsal"];
                        deportista.peso = (decimal)rdr["peso"];
                        deportista.altura = (decimal)rdr["altura"];
                        deportista.paisARepresentar = rdr["paisARepresentar"].ToString();
                    }
                }
            }
            return deportista;
        }
    }

    [WebMethod]
    public string UpdateDeportista(string deportistaID, string primerNombre, string segundoNombre, string primerApellido, string segundoApellido, string fechaNacimiento,
                                    string genero, string dorsal, string peso, string altura) {
        try {
            string query = "UPDATE Deportistas SET " +
                "PrimerNombre='" + primerNombre +
                "', SegundoNombre='" + segundoNombre +
                "', PrimerApellido='" + primerApellido +
                "', SegundoApellido='" + segundoApellido +
                "', FechaNacimiento='" + fechaNacimiento +
                "', Genero='" + genero +
                "', Dorsal=" + dorsal +
                ", Peso=" + peso +
                ", Altura=" + altura +
                " WHERE DeportistaID=" + deportistaID;
            StringBuilder sb = new StringBuilder();
            using (SqlConnection conn = new SqlConnection(connect)) {
                using (SqlCommand cmd = new SqlCommand(query, conn)) {
                    conn.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    return ":3";
                }
            }
        } 
        catch (Exception ex) {
            return ex + "";
        }
    }
    [WebMethod]
    public string DeleteDeportista(string deportistaID) {
        try {
            string query = "UPDATE Deportistas SET " +
                "Activo=0"+
                " WHERE DeportistaID=" + deportistaID;
            StringBuilder sb = new StringBuilder();
            using (SqlConnection conn = new SqlConnection(connect)) {
                using (SqlCommand cmd = new SqlCommand(query, conn)) {
                    conn.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    return ":3";
                }
            }
        } catch (Exception ex) {
            return ex + "";
        }
    }
}


public class Deportista{
    public int deportistaID;
    public string primerNombre;
    public string segundoNombre;
    public string primerApellido;
    public string segundoApellido;
    public DateTime fechaNacimiento;
    public string genero;
    public int dorsal;
    public decimal peso;
    public decimal altura;
    public string foto;
    public string ciudad;
    public string entrenador;
    public string paisARepresentar;
    public int equipoID;
    
}