﻿<%@ WebService Language="C#" Class="FetchEventos" %>
using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Data.SqlClient;
using System.Text;
using System.Web.Script.Services; 


[WebService(Namespace = "http://microsoft.com/webservices/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
[ScriptService]


public class FetchEventos : WebService{
    string connect = "Server=MSI-PC;Database=Patinaje;Trusted_Connection=True";
  [WebMethod]
    public System.Collections.ArrayList GetEventos() {
    System.Collections.ArrayList arreglo = new System.Collections.ArrayList();
    string query = "SELECT * FROM InformacionEventos";
    StringBuilder sb = new StringBuilder();
    using (SqlConnection conn = new SqlConnection(connect)){
        using (SqlCommand cmd = new SqlCommand(query, conn)){
            conn.Open();
            SqlDataReader rdr = cmd.ExecuteReader();
            if (rdr.HasRows){
                while (rdr.Read()){
                    Evento evento = new Evento();
                    evento.eventoID = (int) rdr["EventoID"];
                    evento.fecha = (DateTime) rdr["Fecha"];
                    evento.rama = rdr["Rama"].ToString();
                    evento.tipo = rdr["Tipo"].ToString();
                    evento.prueba = rdr["Prueba"].ToString();
                    evento.sede = rdr["Sede"].ToString();
                    evento.eventoGlobalID = (int) rdr["EventoGlobalID"];
                    arreglo.Add(evento);
                }
            }
        }
        return arreglo;
    }
  }

  [WebMethod]
  public Evento GetEvento(int ID)
  {
      string query = "EXEC EventoByID " + ID.ToString();
      StringBuilder sb = new StringBuilder();
      Evento evento = new Evento();
      using (SqlConnection conn = new SqlConnection(connect))
      {
          using (SqlCommand cmd = new SqlCommand(query, conn))
          {
              conn.Open();
              SqlDataReader rdr = cmd.ExecuteReader();
              if (rdr.HasRows)
              {
                  while (rdr.Read())
                  {
                      evento.eventoID = (int)rdr["EventoID"];
                      evento.fecha = (DateTime)rdr["Fecha"];
                      evento.rama = rdr["Rama"].ToString();
                      evento.tipo = rdr["Tipo"].ToString();
                      evento.prueba = rdr["Prueba"].ToString();
                      evento.sede = rdr["Sede"].ToString();
                      evento.eventoGlobalID = (int)rdr["EventoGlobalID"];
                  }
              }
          }
          return evento;
      }
  }
    
}



public class Evento{
    public int eventoID;
    public DateTime fecha;
    public string rama;
    public string tipo;
    public string prueba;
    public string sede;
    public int eventoGlobalID;
}