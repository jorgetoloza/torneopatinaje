﻿app.controller('perfilCtrl', ['$scope', '$filter', function ($scope, $filter) {
    $scope.deportista = {},
    done = false;
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "scripts/FetchDeportistas.asmx/getDeportista",
        data: "{ ID: '" + $('#perfil').attr('data-id') + "'}",
        dataType: "json",
        success: function (data) {
            $scope.deportista = data.d;
            $scope.deportista.fechaNacimiento = serializeDate($scope.deportista.fechaNacimiento);
            $scope.$digest();
            console.log();
        }
    });
    $scope.$watch('deportista.genero', function (newValue, oldValue) {
        if (newValue) {
            newValue = newValue.toUpperCase();
            if (newValue != "M" && newValue != "F") {
                  $scope.deportista.genero = oldValue.toUpperCase();
            } else {
                $scope.deportista.genero = newValue;
            }
        }
           
    });
    $scope.guardar = function () {
        console.log('guardando');
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "scripts/FetchDeportistas.asmx/UpdateDeportista",
            data: "{" + JSON.stringify($scope.deportista).substring(23),
            dataType: "json",
            success: function (mens) {
                if (mens.d == ':3') {
                    console.log('Deportista #'+$scope.deportista.deportistaID+' actualizado');
                    window.location.href = "deportistas.aspx";
                }
            }
        });
    };
    $scope.eliminar = function () {
        console.log('eliminando');
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "scripts/FetchDeportistas.asmx/DeleteDeportista",
            data: "{deportistaID: '" + $scope.deportista.deportistaID + "'}",
            dataType: "json",
            success: function (mens) {
                if (mens.d == ':3') {
                    console.log('Deportista #'+$scope.deportista.deportistaID+' eliminado');
                    window.location.href = "deportistas.aspx";
                }
            }
        });
    };
}]);