﻿function serializeDate(date) {
    //Remove all non-numeric (except the plus)
    date = date.replace(/[^0-9 +]/g, '');

    //Create date
    return new Date(parseInt(date));
}