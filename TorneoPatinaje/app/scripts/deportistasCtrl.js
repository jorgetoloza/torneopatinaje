﻿app.controller('deportistasCtrl', ['$scope', function ($scope) {
    $scope.deportistas = [];
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "scripts/FetchDeportistas.asmx/getDeportistas",
        success: function (data) {
            $scope.deportistas = data.d;
            for (i = 0; i < $scope.deportistas.length; i++) {
                $scope.deportistas[i].fechaNacimiento = serializeDate($scope.deportistas[i].fechaNacimiento);
            }
            $scope.$digest();
            $scope.$apply(function () {
                $('.deportista').click(function () {
                    window.location.href = "perfil.aspx?deportistaID=" + $(this).attr('data-deportista-id');
                    console.log("perfil.aspx?deportistaID=" + $(this).attr('data-deportista-id'));
                });
            });
            console.log($scope.deportistas);
        }
    });
}]);
